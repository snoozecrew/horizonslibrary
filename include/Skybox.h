#pragma once
#include "Object3D.h"
#include <stdio.h>
#include <vector>
#include "Config.h"

class Skybox :
	public Object3D
{
public:
	Skybox(int);
	~Skybox();

	void update(double deltaTime);

	void draw(RenderSettings*);

	void sortVAO();
	void loadTextures(int);

private:
	GLuint textureID;

	GLuint thisVAO;

	GLuint vertsVBO;

	void applyScaling();

	float scale = 10000.0f;

	std::vector<std::string> facesPaths
	{
			"Textures/Skybox/right.jpg",
			"Textures/Skybox/left.jpg",
			"Textures/Skybox/top.jpg",
			"Textures/Skybox/bottom.jpg",
			"Textures/Skybox/front.jpg",
			"Textures/Skybox/back.jpg"
	};
	std::vector<std::string> facesPaths2
	{
		"Textures/Skybox2/DarkStormyLeft2048.png",
		"Textures/Skybox2/DarkStormyRight2048.png",
		"Textures/Skybox2/DarkStormyUp2048.png",
		"Textures/Skybox2/DarkStormyDown2048.png",
		"Textures/Skybox2/DarkStormyFront2048.png",
		"Textures/Skybox2/DarkStormyBack2048.png"
	};

	float skyboxVertices[36*3] = {
		// positions          
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f,  1.0f
	};
};

