#pragma once
class Volume;
#include "Component.h"
#include "Triangle.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>


typedef uint32_t PhysicsState;

enum PhysicsStates
{
	//Resting. No need to update. Can only be changed if a force is applied in a collision or by a script
	PHYSICS_RESTING = 0b1,
	//Not supported
	PHYSICS_FALLING = 0b10,
	//Supported and gravity does not need to be applied
	PHYSICS_SUPPORTED = 0b100,
	//Does not have gravity applied, in a collision is treated as infinitely large and massive
	PHYSICS_STATIC = 0b1000,
	//No gravity applied
	PHYSICS_WEIGHTLESS = 0b10000,
	//No Collision
	PHYSICS_NO_COLLISIONS = 0b100000
};

struct Force
{
	glm::vec3 magnitude;
	glm::vec3 posRelCG;
	Force(glm::vec3 mag, glm::vec3 pos)
	{
		magnitude = mag;
		posRelCG = pos;
	}
};

class PhysicsComponent :public Component
{
public:
	PhysicsComponent(GameObject * );
	PhysicsComponent(Volume *);
	~PhysicsComponent();

	void setState(PhysicsState);
	void unSetState(PhysicsState);
	bool queryState(PhysicsState);

	//perform movement required for this frame based on values calculated by the physics handler
	void update(double);

	void lateUpdate(double, bool force = false);


	void zero();
	void applyForce(glm::vec3);
	void applyForce(Force);
	void applyAcceleration(glm::vec3);
	void setMass(float m);
	void setPosition(glm::vec3); //to be used sparingly.

	glm::vec3 velocity = glm::vec3(0);
	glm::vec3 forceToApply = glm::vec3(0);
	float mass = 50.0f;
	glm::vec3 position;
	glm::vec3 prevPosition;
	glm::quat rotation;

	bool noHostComp = false;
	Volume * getVol();

	Volume * hostlessVol = nullptr;

	void applyImpulse(glm::vec3);

	float restitution = 0.8f;
	bool needsNewTerrainCalc = true;
	float heightAboveTerrain = 0.0f;
	float heightAtTerrXZ = -1000000.0f;

	bool updatedThisFrame = false;

	bool deleteMe = false;

	float restingOnSurfaceRoughness = 0.0f;

	//1 is super rough, 0 is completely slippery
	float roughness = 0.8f;
private:

	//usage is 1-decay
	const float decay = 0.15f;

	PhysicsState state = 0;
};

