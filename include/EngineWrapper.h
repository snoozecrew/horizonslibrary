#pragma once
class Game;
#include "Config.h"
#include "Scene.h"
#include <iostream>
#define TINYOBJLOADER_IMPLEMENTATION // define this in only *one* .cc
#include "tiny_obj_loader.h"
#include "shader_setup.h"
#include "keyState.h"
#include "Config.h"
#define GLM_SWIZZLE_XYZW
#include <glm-0.9.9.2/glm/glm.hpp>
#include <glad/glad.h>
#include <glfw/glfw3.h>


class EngineWrapper
{
public:
	EngineWrapper();
	~EngineWrapper();	

	void sortConfig();
	void startGame();
	void initGL(int argc, char* argv[], std::string windowName);
	void initGLFW(std::string);
	void shaderInit();
	void addScene(Scene * x);
	void initializeGame(Scene*);

private:
	Config thisConfig;

	void setupFreeType();

	const static int PARAMS_PER_LIGHT = 7;

	std::string propertyNames[PARAMS_PER_LIGHT] =
	{
		"position",
		"colors",
		"attenuation",
		"ambientCoefficient",
		"coneAngle",
		"coneDirection"
		,"finalLightMatrix"
	};

	GLFWwindow* window = nullptr;
};

#ifndef ENGINE_COMPILE
static Game * gameInstanceWrapper;


void idle(void);

void mouseDownWrapper(GLFWwindow* window, int button, int action, int mods);
void mouseMoveWrapper(int x, int y);
void keyEventWrapper(GLFWwindow *, int key, int scancode, int action, int mods);
void specialKey(int Key, int x, int y);
void specialKeyUp(int key, int x, int y);
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void cursor_pos_callback(GLFWwindow* window, double x, double y);
void makeFrameWrapper(void);
#define ENGINE_COMPILE
#endif
