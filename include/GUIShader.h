#pragma once
#include "Shader.h"
class GUIShader :
	public Shader
{
public:
	static GUIShader * getInstance();
	~GUIShader() {}

	void setProj(glm::mat4);

	void setup();
private:
	GUIShader();

	std::string vertexFilename = "Shaders\\vertex_2d.shader";
	std::string fragFilename = "Shaders\\fragment_2d.shader";


	GLuint locProj;

	static GUIShader* thisPointer;
};

