#pragma once
#include "Shader.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#include "LightingState.h"

class DefaultShaders :
	public Shader
{
public:

	~DefaultShaders();

	void setMats(glm::mat4 T, glm::mat4 R, glm::mat4 S, glm::mat4 C);
	void setParentLoc(glm::vec4);
	void setLightUniforms(float,  LightingState*);
	void setTextures();
	void setNormalBool(int);

	static DefaultShaders* getInstance();
	void setInvertY(bool);

	void newFrame() { first = true; }
protected:

	bool first = true;
	int invertY = -1;

	static DefaultShaders * thisPointer;

	void setup();

	DefaultShaders();
	DefaultShaders(int);

	std::string vertexFileName = "Shaders\\vertex_shader.shader";
	std::string fragmentFileName = "Shaders\\fragment_shader.shader";

	GLuint locT = 0;
	GLuint locR = 0;
	GLuint locS = 0;
	GLuint locC = 0;
	GLuint locPosRelP = 0;

	GLuint locInvertY;

	GLuint locShiny;
	GLuint locLightCount;

	GLuint locCameraPos;
	GLuint locUseNormalMap;



	GLuint locTex0;
	GLuint locTex1;
	GLuint locTex2;


	GLuint lightingUniforms[MAX_LIGHTS * PARAMS_PER_LIGHT];

};

