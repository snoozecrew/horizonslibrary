#pragma once
#include "PhysicsHandler.h"
#include "PhysicsComponent.h"
#include "TerrainCollider.h"


class LogicalPhysics :public PhysicsHandler
{
public:
	static LogicalPhysics* getInstance();

	void doPhysics(double, Scene*);

	void checkScene(Scene*);

	RayCastResult castRayDown(glm::vec3 position);

protected:

	static LogicalPhysics* thisPointer;
	LogicalPhysics();
	~LogicalPhysics();

	void doEarlyPhysics();
	void doLatePhysics();

	void handleGravity();

	double timeStep;

	Scene *currScene = nullptr;
};

