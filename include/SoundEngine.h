#pragma once
#include <string>
#include <glm-0.9.9.2\glm\glm.hpp>
#include <oal_ver1.1\include\al.h>
#include <oal_ver1.1\include\alc.h>
#include <oal_ver1.1\include\efx.h>
#include "GURiffModel.h"
#include <mmreg.h>


class SoundEngine
{
public:

	static SoundEngine* getInstance();
	void initialize();

	int addSoundFile(std::string);
	void playSound(int sourceID,int soundID, glm::vec3 pos);
	void loopSound(int sourceID, int soundID);
	void stopSound(int sourceID, int soundID);

private:
	static SoundEngine * thisPointer;
	SoundEngine();
	~SoundEngine();

	std::string soundDir = "Sounds/";


	static const int MAX_SOUNDS = 64;
	int sounds[MAX_SOUNDS] = { -1 };
	int actSounds = 0;
	std::string soundFiles[MAX_SOUNDS] = { "" };
	
	ALuint generateBuffer(std::string);
};

