#pragma once
#include "Shader.h"
class ShadowShaders :
	public Shader
{
public:
	static ShadowShaders* getInstance();


	void setMat(glm::mat4);
private:
	void setup();

	static ShadowShaders* thisPointer;
	ShadowShaders();
	~ShadowShaders();
	GLuint locMVP = 0;

};

