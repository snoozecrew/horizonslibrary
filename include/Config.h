#pragma once
#include "LightingState.h"
#include "Shader.h"
#include "DefaultShaders.h"
#include "SkyboxShaders.h"
#include "GUIShader.h"
#include "WaterShaders.h"
#include "TangentDrawingShaders.h"
#include "NormalDrawingShaders.h"
#include "TextShaders.h"
#include <stdio.h>
#include <map>
#include "TerrainShaders.h"
#include "PostProcessingShader.h"
#include "InstancedShaders.h"
#include "DisplayDetails.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>

class DebugSettings;


struct Character {
	GLuint     textureID;  // ID handle of the glyph texture
	glm::ivec2 Size;       // Size of glyph
	glm::ivec2 Bearing;    // Offset from baseline to left/top of glyph
	GLuint     Advance;    // Offset to advance to next glyph
};


struct Config 
{
	LightingState * currLighting;

	DebugSettings * debugSettings;

	Shader * batchShaders;

	DisplayDetails * displayDetails;

#pragma region Shader pointers

	//Shaders
	DefaultShaders * mainShaders = nullptr;
	WaterShaders * waterShaders = nullptr;
	SkyboxShaders * skyboxShaders = nullptr;
	GUIShader * guiShaders = nullptr;
	TangentDrawingShaders * tanShaders = nullptr;
	NormalDrawingShaders * normalShaders = nullptr;
	TextShaders*  textShaders = nullptr;
	TerrainShaders* terrShaders = nullptr;
	PostProcessingShader * postShaders = nullptr;
	InstancedShaders * instanceShaders = nullptr;


	Shader * customShaders1 = nullptr;
	Shader * customShaders2 = nullptr;
	Shader * customShaders3 = nullptr;
	Shader * customShaders4 = nullptr;

	//Lighting uniforms
	const static int MAX_LIGHTS = 16;
	const static int PARAMS_PER_LIGHT = 7;

#pragma endregion

	std::map<GLchar, Character> arialCharacters;
};




class DebugSettings
{
public:
	DebugSettings() {}

	bool lightSphere = false;
	bool drawTangents = false;
	bool drawNormals = false;

	bool statePrints = false;
};

