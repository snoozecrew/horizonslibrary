#pragma once
#include "VAOData.h"
#include "PrivateBehaviour.h"
#include "InputListener.h"
#include "Scenery.h"

class GameObject;
class Scene;

class GenerateCube : public PrivateBehaviour, public InputListener
{
public:
	GenerateCube(VAOData * data, GameObject * host, Scene * scene);
	~GenerateCube();

	void update(double);

	void OnClick(int key);
private:
	VAOData * shape;
	Custom * cube =nullptr;
	GameObject * player;
	Scene * scene;
	bool deleteTime = false;
};

