#pragma once
#include "PostProcessingShader.h"
#include <iostream>

class ContrastPP :public PostProcessingShader
{
public:
	ContrastPP(float strength)
	{
		fragFilename = "Shaders/PP/contrast_frag.shader";
		s = strength;
		programID = setupShaders(vertexFilename, fragFilename);
		setup();
	}
	void update()
	{
		printf("updating");
		glUniform1f(locStrength, s);
	}
private:
	GLuint locStrength;
	void setup()
	{
		locStrength = glGetUniformLocation(programID, "contrastStrength");
		glUseProgram(programID);
		glUniform1f(locStrength, s);
		glUseProgram(0);
	}
	float s;
};