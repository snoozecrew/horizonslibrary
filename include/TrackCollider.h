#pragma once
#include "Volume.h"
#include "Bounds.h"
#include "Triangle.h"
#include <stdio.h>
#include <vector>

class GameObject;

class TrackCollider :
	public Volume
{
public:
	TrackCollider(Bounds2D, int level, GameObject *);
	TrackCollider(Bounds2D, int level, TrackCollider*, GameObject *);
	~TrackCollider();

	CheckResult checkPoint(glm::vec3);
	TriangleCheckResult checkHeightAtPoint(glm::vec3);
	bool checkVolume(Volume *);
	void addTriangle(Triangle x);

	void clear();

	Bounds2D getBounds() { return bounds; }

	glm::vec3 getMinExtentsWorld();
	glm::vec3 getMaxExtentsWorld() { return glm::vec3(0); }

	TriangleCheckResult getTriangleUnderPoint(glm::vec3);

	HeightCheckResult getHeightAtXZPoint(glm::vec3);

	CheckResult CollideWith(AABBCollider* other);
	CheckResult CollideWith(StaticUprightCylinderVolume* other);
	CheckResult CollideWith(TerrainCollider* other);
	CheckResult CollideWith(PlaneCollider* other);
	CheckResult CollideWith(PointVolume *other);
	CheckResult CollideWith(TrackCollider *other);

	CheckResult CollideWithCollider(Volume * other);
private:

	TriangleCheckResult getHeightAtPointForTriangle(Triangle, glm::vec3);

	void splitIntoNodes();
	void moveAllTrisToNodes();
	void tryMoveTriangleToNode(Triangle);


	TrackCollider * parent = nullptr;

	int thisLevel = 0;

	Bounds2D bounds;
	TrackCollider * nodes[4] = { nullptr };


	bool split = false;
	int triCount = 0;
	const static int MAX_TRIS_PER_NODE = 25;
	const static int MAX_LEVELS = 11;
	std::vector<Triangle> tris;
	std::vector<Triangle> trisToRetain;
	int trisSeen = 0;
};

