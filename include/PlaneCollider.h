#pragma once
#include "Volume.h"

class PlaneCollider :
	public Volume
{
public:
	PlaneCollider(GameObject * host);
	~PlaneCollider();


	CheckResult checkPoint(glm::vec3);
	bool checkVolume(Volume *);
	glm::vec3 getMinExtentsWorld();
	glm::vec3 getMaxExtentsWorld();
	HeightCheckResult getHeightAtXZPoint(glm::vec3);

	CheckResult CollideWith(AABBCollider* other);
	CheckResult CollideWith(StaticUprightCylinderVolume* other);
	CheckResult CollideWith(TerrainCollider* other);
	CheckResult CollideWith(PlaneCollider* other);
	CheckResult CollideWith(PointVolume *other);
	CheckResult CollideWith(TrackCollider *other);

	CheckResult CollideWithCollider(Volume * other);
};

