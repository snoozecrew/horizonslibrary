#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtc/type_ptr.hpp>
#include "Config.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "RenderSettings.h"
#include "Shader.h"
#include "PrivateBehaviour.h"
#include "ComponentReference.h"
#include "Extents.h"
#include "Volume.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>

class GameObject
{
public:
	GameObject()
	{
		componentReference = new ComponentReference();
	}

	virtual ~GameObject()
	{
		delete volume;
		for (int count = 0; count < MAX_BEHAVIOURS; count++)
		{
			if (privateBehaviours[count] != nullptr) delete privateBehaviours[count];
		}
		delete componentReference;
	}

	virtual void update(double deltaTime);
	virtual void draw(RenderSettings*) = 0;

	//return the position
	glm::vec3 getPos()
	{return glm::vec3(posX, posY, posZ);}

	//return the rotation
	glm::vec3 getRot()
	{return glm::vec3(thetaX, thetaY, thetaZ);}

	glm::vec3 getScale()
	{return glm::vec3(scaleX, scaleY, scaleZ);}

	glm::mat4 getRotMat()
	{
		return glm::toMat4(
			glm::quat(glm::vec3(0.0f, thetaY, 0.0f))*
			glm::quat(glm::vec3(thetaX, 0.0f, 0.0f))*
			glm::quat(glm::vec3(0.0f, 0.0f, thetaZ)));
	}

	glm::mat4 getTransMat()
	{
		return glm::translate(glm::mat4(1), glm::vec3(posX, posY, posZ));
	}

	glm::mat4 getScaleMat()
	{
		return glm::scale(glm::mat4(1), glm::vec3(scaleX, scaleY, scaleZ));
	}

	glm::mat4 getModelMat()
	{
		return getTransMat() * getRotMat() * getScaleMat();
	}

	void setPos(glm::vec3 pos)
	{
		posX = pos.x;
		posY = pos.y;
		posZ = pos.z;
	}
	void setRot(glm::vec3 pos)
	{
		thetaX = pos.x;
		thetaY = pos.y;
		thetaZ = pos.z;
		R = getRotMat();
		prevRot = pos;
	}
	void setScale(glm::vec3 pos)
	{
		scaleX = pos.x;
		scaleY = pos.y;
		scaleZ = pos.z;
	}

	void setConfig(Config newConfig) { thisConfig = newConfig;}

	Config getConfig() { return thisConfig; }

	Volume * getVolume() {	return volume;}

	void setVolume(Volume * v) { volume = v; }

	void setParent(GameObject* newParent) { parent = newParent;  }

	void addPrivateBehaviour(PrivateBehaviour* x)
	{
		for (int count = 0; count < MAX_BEHAVIOURS; count++)
		{
			if (privateBehaviours[count] == nullptr)
			{
				privateBehaviours[count] = x;
				x->setTarget(this);
				return;
			}
		}
	}

	void updateBehaviours(double delta)
	{
		for (int count = 0; count < MAX_BEHAVIOURS; count++)
		{
			if (privateBehaviours[count] != nullptr)

			{
				privateBehaviours[count]->update(delta);
			}
		}
	}

	Shader * getShader() { return shader; }
	VAOData * getVAOData() { return dataVAO; }

	//returns the extents timed by the scale and the transformation. Please fix to include rotation
	Extents getWorldExtents();

	ComponentReference * componentReference = nullptr;
	std::string name = "";
	Extents e;
	void kill() { dead = true; }

protected:
	
	bool dead = false;
#pragma region Transform modifiers
	float posX = 0.0f;
	float posY = 0.0f;
	float posZ = 0.0f;

	glm::vec3 prevPos;

	float thetaX = 0.0f;
	float thetaY = 0.0f;
	float thetaZ = 0.0f;

	glm::vec3 prevRot;

	float scaleX = 1.0f;
	float scaleY = 1.0f;
	float scaleZ = 1.0f;

	glm::vec3 prevScale;
#pragma endregion

	Shader * shader=nullptr;

	VAOData * dataVAO =nullptr;

	Config thisConfig;

	GameObject * parent = nullptr;

	GLuint defaultShaders = 0;

	const static int MAX_BEHAVIOURS = 24;
	PrivateBehaviour * privateBehaviours[MAX_BEHAVIOURS] = { nullptr };

	Volume * volume = nullptr;


	glm::mat4 R = glm::toMat4(glm::quat(glm::vec3(0.0f, 0.0f, 0.0f)));
	glm::mat4 pR = glm::toMat4(glm::quat(glm::vec3(0.0f, 0.0f, 0.0f)));

	glm::mat4 T = glm::mat4();
	glm::mat4 pT = glm::translate(glm::mat4(), glm::vec3(0, 0, 0));

	glm::mat4 S = glm::mat4(1);

};

