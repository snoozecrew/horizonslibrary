#pragma once
#include "Camera.h"
class ThirdPersonCamera :
	public Camera
{
public:
	ThirdPersonCamera();
	~ThirdPersonCamera();
	void update(double deltaTimed, int, int, bool playing);
	glm::mat4 getLookAtMatrix(bool invertY, glm::vec3 pos);
};

