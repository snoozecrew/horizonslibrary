#pragma once
#include "PublicBehaviour.h"
#include "Text.h"
#include "Volume.h"
class Game;
class CheckPlayerHeight :
	public PublicBehaviour
{
public:
	CheckPlayerHeight(Game *, GameObject*);
	~CheckPlayerHeight();

	void update(double);

private:
	Game * refr;


	GameObject* player;

	Text * tex;
};

