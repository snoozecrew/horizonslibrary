#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>

class TerrainTextureData
{
public:
	GLuint bases[5];
	GLuint normals[5];
	int normalInvertY[5] = { -1 };
	GLuint specs[5];
	GLuint splat;
};