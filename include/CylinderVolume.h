#pragma once
#include "Volume.h"

struct UprightCylinderExtents
{
	UprightCylinderExtents operator * (glm::mat4 mat) const
	{
		UprightCylinderExtents e;
		e.centre = mat * centre;
		e.minY = mat * minY;
		e.maxY = mat * maxY;
		e.radius = radius;

		e.minY.x = 0.0f;
		e.minY.z = 0.0f;
		e.maxY.x = 0.0f;
		e.maxY.z = 0.0f;
		return e;
	}


	glm::vec4 centre;
	glm::vec4 minY;
	glm::vec4 maxY;
	//radius is stored in x
	glm::vec4 radius;


};

class StaticUprightCylinderVolume :public Volume
{
public:
	StaticUprightCylinderVolume(GameObject * host, int, glm::vec4 c, float minY, float maxY, float r);
	StaticUprightCylinderVolume(GameObject * host, int, glm::vec3 c, float minY, float maxY, float r);
	~StaticUprightCylinderVolume();

	CheckResult checkPoint(glm::vec3);
	bool checkVolume(Volume *);
	glm::vec3 getMinExtentsWorld();
	glm::vec3 getMaxExtentsWorld();
	HeightCheckResult getHeightAtXZPoint(glm::vec3);

	CheckResult CollideWith(AABBCollider* other);
	CheckResult CollideWith(StaticUprightCylinderVolume* other);
	CheckResult CollideWith(TerrainCollider* other);
	CheckResult CollideWith(PlaneCollider* other);
	CheckResult CollideWith(PointVolume *other);
	CheckResult CollideWith(TrackCollider *other);

	CheckResult CollideWithCollider(Volume * other);


	UprightCylinderExtents getWorldExtents();

private:
	UprightCylinderExtents localExtents;
};
