#pragma once
#include "DefaultShaders.h"
class WorldGUIShader :
	public DefaultShaders
{
public:
	static WorldGUIShader* getInstance();
protected:

	static WorldGUIShader* thisPointer;
	WorldGUIShader();
	~WorldGUIShader();

	std::string vertFileName = "Shaders/vertex_shader_worldGUI.shader";
	std::string fragFileName = "Shaders/fragment_shader_worldGUI.shader";

	void setup();
};

