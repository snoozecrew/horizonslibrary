#pragma once
#include "FBO.h"
class ShadowFBO :
	public FBOGroup
{
public:
	ShadowFBO(int width, int height);
	~ShadowFBO();

	void update();
private:
	void setupFBO();


	int w;
	int h;

	GLuint frameBuffer = 0;

	GLuint texture = 0;
};

