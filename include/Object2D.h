#pragma once
#include "GameObject.h"
#include <stdio.h>
#include <string>
#include <FreeImage\FreeImagePlus.h>
#include "texture_loader.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>

class Object2D : public virtual GameObject
{
public:
	
	Object2D();
	~Object2D() {}
	virtual void draw(RenderSettings*) {}
	virtual void update(double delta) { deltaTime = delta; }
protected:

	//Texture containers
	GLuint texture = 0;


	double deltaTime;

};