#pragma once
#include "Object2D.h"
#include "Text.h"
#include <string>
#include <stdio.h>
class Game;
class FPSCounter :
	public Object2D
{
public:
	FPSCounter(Game * refr);
	~FPSCounter();
	void update(double);
	void draw(RenderSettings*);
	void setConfig(Config newConfig) { thisConfig = newConfig;if (text!= nullptr) text->setConfig(newConfig); }
private:
	Text* text =nullptr;
	Game * refr;
};

