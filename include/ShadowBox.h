#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#include <vector>
class Camera;
class Light;
class ShadowBox
{
public:
	ShadowBox(Light * , Camera * camera);
	~ShadowBox();

	void setCamera(Camera*x) { cam = x; }

	void update();
private:
	
	Light * light;

	glm::vec3 getCenter();

	float OFFSET = 10;
	glm::vec4 UP = glm::vec4(0, 1, 0, 0);
	glm::vec4 FORWARD = glm::vec4 (0, 0, -1, 1);
	float SHADOW_DISTANCE = 100;

	float minX, maxX;
	float minY, maxY;
	float minZ, maxZ;
	glm::mat4 lightViewMatrix;
	Camera* cam;

	float farHeight, farWidth, nearHeight, nearWidth;

	float nearPlaneMultiplier = 1.0f;

	float getWidth();

	float getHeight();
	
	float getLength();

	std::vector<glm::vec4> calculateFrustumVertices(glm::mat4 rotation, glm::vec3 forwardVector, glm::vec3 centerNear, glm::vec3 centerFar);

	glm::mat4 calculateCameraRotationMatrix();

	glm::vec4 calculateLightSpaceFrustumCorner(glm::vec3 startPoint, glm::vec3 direction, float width);

	void calculateWidthsAndHeights();
};

