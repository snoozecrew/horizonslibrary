#pragma once
#include "PhysicsHandler.h"
#include "PhysicsComponent.h"
class CollisionBasedPhysics :
	public PhysicsHandler
{
public:

	static CollisionBasedPhysics* getInstance();

	void doPhysics(double, Scene*);

	void checkScene(Scene*);

	RayCastResult castRayDown(glm::vec3 position) { RayCastResult x; return x; }
protected:
	static CollisionBasedPhysics* thisPointer;

	CollisionBasedPhysics();
	~CollisionBasedPhysics();

	void doEarlyPhysics();
	void doLatePhysics();

	void handleGravity();
	void handleCollisions();

	void takeCollisionAction(PhysicsComponent* first, PhysicsComponent* second, CheckResult cr);

	double timeStep;

	Scene *currScene = nullptr;
};

