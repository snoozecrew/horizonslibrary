#pragma once
#include "SceneRenderer.h"
#include "PostProccessingFBO.h"
#include "PostProcessingShader.h"
#include "PostProcessRect.h"

class LogicalRenderer :
	public SceneRenderer
{
public:
	LogicalRenderer();
	~LogicalRenderer();
	void update(double);

	void render();
	void initalize();

	void addPostProcessor(PostProcessingShader *);
private:


	//Go through each FBO group needed
	void renderToEachFBOGroup();
	//Render to a FBO group
	void renderToFBOGroup(ReflectiveObject*);
	void renderToFBOGroup(FBOGroup*);
	//render thescene to a specific camera angle
	void renderSceneFBO(RenderSettings*, ReflectiveObject*);
	void renderSceneFBO(RenderSettings*, FBOGroup*);
	//render the scene
	void renderScene(glm::mat4);
	//render just the game objects to a specific camera angle
	void renderGameObjects(RenderSettings*);
	//render just the transparent objects to a specific camera angle
	void renderTransparentObjects(RenderSettings*);
	//render any reflective object
	void renderReflectiveObjects(RenderSettings*, ReflectiveObject*);
	//render light spheres
	void renderLights(RenderSettings *, Light*);
	//render GUI elements
	void renderGUI(RenderSettings *);
	//render the result of the post processing
	void doPostProcessing();

	void renderShadowMap( Light * light);


#pragma region Post Processing
	PostProcessRect * finalRender = nullptr;
	PostProcessRect * partialRender = nullptr;

	PostProccessingFBO * ppFBO = nullptr;

	PostProccessingFBO * msFBO = nullptr;

	const static int MAX_PP_SHADERS = 5;

	int ppCount = 0;

	GLuint textureResults[MAX_PP_SHADERS + 1];

	PostProcessingShader * ppShaders[MAX_PP_SHADERS] = { nullptr };
	PostProccessingFBO * auxPPFBOs[MAX_PP_SHADERS] = { nullptr };
#pragma endregion
};

