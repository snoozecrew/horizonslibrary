#pragma once
#include "TerrainCollider.h"

class Scene;
class PhysicsComponent;

struct RayCastResult
{
	float distanceTravelled;
	glm::vec3 hitPos;
};


class PhysicsHandler
{
public:
	PhysicsHandler();
	~PhysicsHandler();

	virtual void doPhysics(double, Scene*) = 0;
	virtual void checkScene(Scene*)= 0;

	void setG(float x) { g = x; }

	virtual RayCastResult castRayDown(glm::vec3 position) = 0;

protected:

	const static int MAX_PHYSICS_COMPONENTS = 2048;
	PhysicsComponent * objectsWithPhysics[MAX_PHYSICS_COMPONENTS] = { nullptr };
	int actObjects = 0;

	//Terrains are simply used to specify a minimum y value at any given x,z. There is no movement of them.
	const static int MAX_TERRAINS = 16;
	TerrainCollider * terrains[MAX_TERRAINS] = { nullptr };
	int actTerrains = 0;


	const static int MAX_STATIC_PHYSICS_COMPONENTS = 64;
	PhysicsComponent * objectsWithStaticPhysics[MAX_STATIC_PHYSICS_COMPONENTS] = { nullptr };
	int actStatics = 0;

	float g = 9.81f;
};

