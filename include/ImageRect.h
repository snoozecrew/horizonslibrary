#pragma once
#include "Config.h"
#include "Object2D.h"

class ImageRect :
	public Object2D
{
public:
	ImageRect(GLuint);
	ImageRect(GLuint, glm::vec2);
	ImageRect();
	~ImageRect();
	void sortVAO();
	void draw(RenderSettings*);
	void setTexture(GLuint x) { texture = x; }
	void applyScaling();

protected:
	const int totalPoints = 8;

	float points[8] =
	{
		-1.0f,1.0f,
		1.0f,1.0f,
		1.0f,-1.0f,
		-1.0f,-1.0f
	};	

	float texts[8] =
	{
		0.0,1.0f,
		1.0f,1.0f,
		1.0f,0.0f,
		0.0f,0.0f
	};

	float scaleX;

	GLuint VAO = 0;

	GLuint pointsVBO = 0;
	GLuint textsVBO = 0;


	glm::vec2 res= glm::vec2( 50.0f);

};

