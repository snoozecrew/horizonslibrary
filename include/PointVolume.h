#pragma once
#include "Volume.h"
#include "Extents.h"
class PointVolume :
	public Volume
{
public:
	PointVolume(GameObject*, Extents e);
	~PointVolume();
	CheckResult checkPoint(glm::vec3);
	bool checkVolume(Volume *);
	glm::vec3 getMinExtentsWorld();
	glm::vec3 getMaxExtentsWorld();
	HeightCheckResult getHeightAtXZPoint(glm::vec3);

	CheckResult CollideWith(AABBCollider* other);
	CheckResult CollideWith(StaticUprightCylinderVolume* other);
	CheckResult CollideWith(TerrainCollider* other);
	CheckResult CollideWith(PlaneCollider* other);
	CheckResult CollideWith(PointVolume *other);
	CheckResult CollideWith(TrackCollider *other);

	CheckResult CollideWithCollider(Volume * other);
private:

	Extents extents;
};

