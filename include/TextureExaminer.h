#pragma once
#include <glad/glad.h>
#include <glfw/glfw3.h>
#include <stdio.h>
#include <vector>
#include <string>

using namespace std;

class TextureExaminer
{
public:
	TextureExaminer();
	~TextureExaminer();

	GLuint getTexture(string);
	GLuint getNormalTexture(string);

	void setDirectory(string x) { directory = x; }

private:
	const static int MAX_TEXTURES = 64;
	GLuint textureIDs[MAX_TEXTURES] = { 0 };

	std::vector<string> textureNamesDone;

	void readInNewTexture(int index, string filename);
	void readInNewNormalTexture(int index, string filename);

	string removeLongFileName(string);

	string directory = "";
};

