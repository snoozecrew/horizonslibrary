#pragma once
#include "Object2D.h"
#include <ft2build.h>
#include FT_FREETYPE_H

class TextRect :
	public Object2D
{
public:
	TextRect();
	~TextRect();
	void sortVAO();
	void draw(RenderSettings*);
	void setScale(float nx) { scaleX = nx; applyScaling(); }


protected:
	const int totalPoints = 8;

	float points[8] =
	{
		-1.0f,1.0f,
		1.0f,1.0f,
		1.0f,-1.0f,
		-1.0f,-1.0f
	};

	float texts[8] =
	{
		0.0,1.0f,
		1.0f,1.0f,
		1.0f,0.0f,
		0.0f,0.0f
	};


	float scaleX;

	GLuint VAO = 0;

	GLuint pointsVBO = 0;
	GLuint textsVBO = 0;
	void applyScaling();
};

