#pragma once
#include "Volume.h"
#include <vector>
#include "Bounds.h"
#include "Triangle.h"
#include "GameObject.h"



//A terrain collider assumes if the object is above the terrain, there is no collision. Therefore, it does not work 
//with over hangs. This can be improved in the future please

//This collider works by storing the details of every triangle in the terrain. So that this doesn't take an absolute age,
//they are stored in a quad tree. Each node can be in two states, if split = false then it can store a fixed number triangles.
//if this is exceeded, it splits itself into 4 and distributes all it's triangles between the new nodes. Now, it will not store
//most triangles, simply redirect any incoming triangle to the correct one of it's nodes, unless the triangle is too big to fit into a
//smaller node. 

class TerrainCollider :
	public Volume
{
public:
	TerrainCollider(Bounds2D, int level, GameObject *);
	TerrainCollider(Bounds2D, int level, TerrainCollider*, GameObject *);
	~TerrainCollider();

	CheckResult checkPoint(glm::vec3);
	TriangleCheckResult checkHeightAtPoint(glm::vec3);
	bool checkVolume(Volume *);
	void addTriangle(Triangle x);

	void clear();

	Bounds2D getBounds() { return bounds; }

	void reviewNodes();

	std::pair<int, int> getTotalTris(std::pair<int, int>);

	glm::vec3 getMinExtentsWorld();
	glm::vec3 getMaxExtentsWorld() { return glm::vec3(0); }

	TriangleCheckResult getTriangleUnderPoint(glm::vec3);

	HeightCheckResult getHeightAtXZPoint(glm::vec3);

	CheckResult CollideWith(AABBCollider* other);
	CheckResult CollideWith(StaticUprightCylinderVolume* other);
	CheckResult CollideWith(TerrainCollider* other);
	CheckResult CollideWith(PlaneCollider* other);
	CheckResult CollideWith(PointVolume *other);
	CheckResult CollideWith(TrackCollider *other);

	CheckResult CollideWithCollider(Volume * other);
private:

	TriangleCheckResult getHeightAtPointForTriangle(Triangle, glm::vec3);

	void splitIntoNodes();
	void moveAllTrisToNodes();
	void tryMoveTriangleToNode(Triangle);


	TerrainCollider * parent = nullptr;

	int thisLevel = 0;

	Bounds2D bounds;
	TerrainCollider * nodes[4] = { nullptr };


	bool split = false;
	int triCount = 0;
	const static int MAX_TRIS_PER_NODE = 25;
	const static int MAX_LEVELS = 11;
	std::vector<Triangle> tris;
	std::vector<Triangle> trisToRetain;
	int trisSeen = 0;
};

