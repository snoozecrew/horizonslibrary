#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#define _USE_MATH_DEFINES
#include <math.h>

struct Bounds2D
{
	Bounds2D() { bottomLeft = glm::vec2(-100000000); topRight = glm::vec2(10000000); }

	Bounds2D(glm::vec2 bL, glm::vec2 tR) : bottomLeft(bL), topRight(tR){
		width = topRight.x - bottomLeft.x;
		height = topRight.y - bottomLeft.y;
	}
	//Convert from vec3, using XZ
	Bounds2D(glm::vec3 bL, glm::vec3 tR){
		bottomLeft = glm::vec2(bL.x, bL.z);
		topRight = glm::vec2(tR.x, tR.z);
		width = topRight.x - bottomLeft.x;
		height = topRight.y - bottomLeft.y;
	}
	glm::vec2 bottomLeft;
	glm::vec2 topRight;
	double width;
	double height;
};