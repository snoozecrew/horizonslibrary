#pragma once
#include "Volume.h"
#include "Extents.h"

class AABBCollider :
	public Volume
{
public:
	AABBCollider(glm::vec3 min, glm::vec3 max, GameObject*);
	AABBCollider(GameObject*);
	~AABBCollider();

	void preCheckCalcs(Volume *) ;
	CheckResult checkPoint(glm::vec3);
	bool checkVolume(Volume *);
	void createVolume(glm::vec3 centre);
	glm::vec3 getMinExtentsWorld();
	glm::vec3 getMaxExtentsWorld();

	HeightCheckResult getHeightAtXZPoint(glm::vec3);

	CheckResult CollideWith(AABBCollider* other);
	CheckResult CollideWith(StaticUprightCylinderVolume* other);
	CheckResult CollideWith(TerrainCollider* other);
	CheckResult CollideWith(PlaneCollider* other);
	CheckResult CollideWith(PointVolume *other);
	CheckResult CollideWith(TrackCollider *other);

	CheckResult CollideWithCollider(Volume * other);

	Extents getWorldExtents();
protected:
	Extents extents;
};

