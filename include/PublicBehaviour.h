#pragma once
#include "Behaviour.h"
class Scene;
class GameObject;
class PublicBehaviour
{
public:
	PublicBehaviour() {}
	~PublicBehaviour() {}
	void setTarget(Scene* newTarget) { sceneTarget = newTarget; }
	virtual void update(double) = 0;

protected:
	Scene * sceneTarget;
};
