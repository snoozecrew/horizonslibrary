#pragma once
#include "Light.h"
class Moonlight : public Light
{
public:
	Moonlight( glm::vec3 pos)
	{
		position = glm::vec4(pos, 0.0f);
		attenuation = 0.01f;
		ambientCoefficient = 0.0001f;

		coneDirection = glm::vec3(0.0f, -1.0f, 0.0f);
		intensities = glm::vec3(0.2f, 0.2f, 0.2f);
	}


};


class Daylight : public Light
{
public:
	Daylight(glm::vec3 pos)
	{
		position = glm::vec4(pos, 0.0f);
		attenuation = 0.01f;
		ambientCoefficient = 0.06f;

		coneDirection = glm::vec3(0.0f, -1.0f, 0.0f);
		intensities = glm::vec3(1.8f, 1.8f, 1.5f);
	}


};