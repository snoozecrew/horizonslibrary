#pragma once
#include "Object2D.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#include "Config.h"
class Text :
	public Object2D
{
public:
	Text();
	~Text();
	void update(double x);

	void draw(RenderSettings*);

	void sortVAO();

	void setText(std::string x) { text = x; }

private:

	std::string text = "Hello World";
	glm::mat4 projection;

	GLuint VBO = 0;
	GLuint VAO = 0;

	float scale =0.5f;

	glm::vec3 colour = glm::vec3(1.0f, 1.0f, 1.0f);

	float yOffset = 55.0f;
};

