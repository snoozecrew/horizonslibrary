#pragma once
#include "PrivateBehaviour.h"
#include "Game.h"

class ForceOnKey :public PrivateBehaviour
{
public:
	ForceOnKey(Keystate key, glm::vec3 force)
	{
		k = key;
		f = force;
	}


	void update(double x)
	{
		if (Game::getInstance()->getKeys() & k)
		{
			if (target->componentReference->physics != nullptr)
				target->componentReference->physics->applyForce(f);
			else
				std::cout << "the attached object has no physics component";
		}
	}

private:
	Keystate k;
	glm::vec3 f;
};