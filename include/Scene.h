#pragma once
#include "ObjExaminer.h"
#include "GameObject.h"
#include "Config.h"
#include "LightingState.h"
#include "ReflectiveObject.h"
#include "PublicBehaviour.h"
#include "InputListener.h"
#include "Waypoint.h"
#include "Terrain.h"
#include <string>

class Game;

#define SCENE_TERRAIN_PTR 0

class Scene
{
public:
	Scene() {}
	~Scene() {}
	std::string name;
	//There are some requirments for a scene to run in the next 3 methods
	virtual void run() = 0;
	//Run HAS to set the playing state, and the player pointer
	virtual void load(Game* thisGame) = 0;
	//load has to call loadtextures, and set the lighting state of the game
	void loadTextures() {}

	void loadInit();

	std::vector<GameObject*> getObjects() { return renderableObjects; }
	std::vector<GameObject*> getTransObjects() { return transparentRenderableObjects; }
	std::vector<GameObject*> getGUIObjects() { return guiRenderableObjects; }
	std::vector<ReflectiveObject*> getReflectiveObjects() { return reflectiveRenderableObjects; }
	std::vector<PublicBehaviour*> getPublicBehaviours() { return publicBehavioursList; }
	std::vector<InputListener*> getListeners() { return inputListenersList; }
	std::vector<Waypoint *> getWaypoint() { return waypointsList; }
	std::vector<Volume*> getVolumes() { return volumes; }

	void addGameObject(GameObject*);
	void addTransparentGameObject(GameObject*);
	void addReflectiveGameObject(ReflectiveObject* newObject);
	void addPublicBehaviour(PublicBehaviour*);
	void addGUIObject(GameObject*);
	void addWaypoint(Waypoint *);
	void addVolume(Volume*);

	LightingState * currLighting;
	DebugSettings * debugSettings;

	void removeGameObject(GameObject *);

	void addInputListener(InputListener *);

	void setGameObjectPointer(int, GameObject*);

	Waypoint* findWaypoint(std::string);
	GameObject * terrain = nullptr;

	void endOfFrame();

protected:
	Game * gameRefr;

	ObjExaminer * examiner;

	std::vector<GameObject*> renderableObjects;

	std::vector<GameObject*> transparentRenderableObjects;

	std::vector<ReflectiveObject*> reflectiveRenderableObjects;

	std::vector<PublicBehaviour*> publicBehavioursList;

	std::vector<GameObject*> guiRenderableObjects;

	std::vector<Waypoint*> waypointsList;

	std::vector<InputListener*> inputListenersList;

	std::vector<GameObject*> removeEndOfFrame;

	std::vector<Volume*> volumes;
};

