#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#include "Bounds.h"
#include "Triangle.h"
#define _USE_MATH_DEFINES
#include "Extents.h"
#include <math.h>
class GameObject;

glm::vec2 makeVec3xzVec2(glm::vec3 x);

glm::vec3 makeVec3Vec4IgnoreW(glm::vec4);

static glm::vec2 returny;
static glm::vec3 returny3;

float heightAboveTriangleGivenXZ(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec2 pos);
float heightAboveTriangleGivenXZ(Triangle tri, glm::vec2 pos);

glm::vec3 getAverageOf3PointsXZ(glm::vec3, glm::vec3, glm::vec3);

bool isWithinBoundsXZ(glm::vec3 p, Bounds2D b);

bool triExceedsBounds(Triangle x, Bounds2D b);
bool extentsExceedsBounds(Extents e, Bounds2D b);

bool isPointInTriangle(glm::vec2 pt, glm::vec2 v1, glm::vec2 v2, glm::vec2 v3);
bool isPointInTriangle(glm::vec2 pt, Triangle);

static glm::vec2 v1;
static glm::vec2 v2;
static glm::vec2 v3;

float sign(glm::vec2 p1, glm::vec2 p2, glm::vec2 p3);

void gameobjectLookAt(GameObject * host,  GameObject * target, float maxTurn =2* M_PI);
