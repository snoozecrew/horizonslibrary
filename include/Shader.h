#pragma once
#include "shader_setup.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <sstream>
#include <string>
#include <stdio.h>
#include <glm-0.9.9.2/glm/glm.hpp>

class Shader
{
public:
	Shader();
	~Shader();
	virtual void setup() = 0;
	GLuint getProgramID();
	void setClipPlane(glm::vec4, bool);
	virtual void newFrame() {}
protected:
	const static int MAX_LIGHTS = 16;
	const static int PARAMS_PER_LIGHT = 7;
	GLuint programID;
	GLuint getLightUniformLocationName(int count, int index);

	std::string propertyNames[PARAMS_PER_LIGHT] =
	{
		"position",
		"colors",
		"attenuation",
		"ambientCoefficient",
		"coneAngle",
		"coneDirection"
		,"finalLightMatrix"
	};

	GLuint locCP;
	GLuint locCPbool;
};

