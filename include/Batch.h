#pragma once

#include <glm-0.9.9.2/glm/glm.hpp>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
class Batch
{
public:
	Batch();
	~Batch();
	
	//VBOs
	GLuint vertsVBO; //contains a vec4, xyz plus the textureindex (which of the 16 textures is diff)
	GLuint normalsVBO; //contains a vec4, normal xyz plus the textureindex for spec
	GLuint texCoordsVBO; //contains a vec3, tex uv plus the textureindex for normal
	GLuint tangentsVBO; //contains a vec4, tan xyz plus textureindex for 4th tex. Rest are as expected
	GLuint bitangentsVBO;
	GLuint transformIndexVBO;
	GLuint indiciesVBO;

	//the VAO for this batch
	GLuint batchVAO;

	//16 textures
	int textures[16] = { -1 };

	//containers for the matricies for this batch
	const static int matsPerBatch = 64;
	glm::mat4 transforms[matsPerBatch];
};

