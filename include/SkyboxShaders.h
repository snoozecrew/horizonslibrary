#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#include "Shader.h"

class SkyboxShaders :
	public Shader
{
public:
	static SkyboxShaders * getInstance();
	~SkyboxShaders();

	void setMats(glm::mat4 finalTrans, glm::mat4 C, glm::mat4 P);

	void setScale(float x);

private:
	SkyboxShaders();
	static SkyboxShaders* thisPointer;

	void setup();

	std::string vertexFilename = "Shaders\\vertex_shader_skybox.shader";
	std::string fragmentFilename = "Shaders\\fragment_skybox.shader";

	GLuint locPs;
	GLuint locCs;
	GLuint locScale = 0;
	GLuint locTransform = 0;
};

