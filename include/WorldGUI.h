#pragma once
#include "Object3D.h"
#include "ImageRect.h"
class WorldGUI :
	public Object3D, public ImageRect
{
public:
	WorldGUI(GLuint texture, glm::vec2 res);
	~WorldGUI();
	void update(double);
	void draw(RenderSettings*);

	static const int unitsPerPixel = 10;
protected:
	void applySizing();

	
};

