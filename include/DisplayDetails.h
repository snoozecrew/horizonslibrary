#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>

class DisplayDetails
{
public:
	int height = 1080;
	int width = 1920;

	float aaAmount = 0;

	void update()
	{
		
	}

	float getAspectRatio() { return width / height; }
};