#pragma once
#include <FreeImage\FreeImagePlus.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "GameObject.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#include "keyState.h"
#include "Scene.h"
#include "Config.h"
#include "ObjExaminer.h"
#include "Clock.h"
#include "Camera.h"
#include "LightingState.h"
#include "Light.h"
#include <oal_ver1.1/include/alc.h>
#include <oal_ver1.1/include/al.h>
#include "RenderSettings.h"
#include "PostProccessingFBO.h"
#include "ReflectiveObject.h"
#include "PostProcessRect.h"
#include "ShadowBox.h"
#include "SceneRenderer.h"
#include "LogicalRenderer.h"
#include "PhysicsHandler.h"
#include "LogicalPhysics.h"

class Custom;

class Game
{
public:

	static Game * getInstance();
	void setup(Config newConfig, Scene * firstScene);


	void Start();

	void render(void);
	void update(void);
	void setConfigAll();

	void addGameObject(GameObject*);
	void addTransparentGameObject(GameObject*);
	void addReflectiveGameObject(ReflectiveObject *);
	void addGUIObject(GameObject*);

	LightingState* getLighting() { return thisConfig.currLighting; }
	void setLighting(LightingState* newLighting) {
		thisConfig.currLighting = newLighting; }

	void setCamera(Camera *newCamera) { gameCamera = newCamera; thisConfig.currLighting->currCamera = newCamera; renderEngine->setCamera(gameCamera); }
	void setPlaying(bool tf) { playing = tf; }

	Config getConfig() { return thisConfig; }


#pragma region Input Callbacks
	void mouseDown(int buttonID, int state, int mods);
	void mouseMove(int x, int y);
	void keyEvent(int key, int scancode, int action, int mods);
	void mousePassiveMouse(double x, double y);
#pragma endregion

	Keystate getKeys();

	void setPlayer(GameObject* newPlayer) { player = newPlayer; }

	Config thisConfig;

#pragma region Scene Stuff
	void runNextScene()
	{
		currScene++;

		scenes[currScene]->run();

		setConfigAll();

		renderEngine->setScene(scenes[currScene]);
	}

	void loadScene(char * name)
	{
		for (int i = 0; i < MAX_SCENES; i++)
		{
			if (scenes[i] != nullptr)
			{
				if (scenes[i]->name == name)
				{	
					//Free up the current Scene
					scenes[currScene] = nullptr;

					//Reassign the current scene 
					currScene = i;
					if (thisConfig.debugSettings->statePrints) printf("about to call scene.load\n");
					//Load this next Scene (please move into a thread)
					scenes[i]->load(this);
					if (thisConfig.debugSettings->statePrints) printf("about to call scene.run\n");
					scenes[i]->run();


					renderEngine->setScene(scenes[currScene]);
					return;
				}
			}
		}
		printf("Scene Not Found");
	}

	void loadNextScene()
	{
		scenes[currScene+1]->load(this);
		runNextScene();
	}

	void addScene(Scene *x)
	{
		for(int count = 0; count < MAX_SCENES; count++)
		{
			if (scenes[count] == nullptr)
			{
				scenes[count] = x;
				return;
			}
		}
	}

#pragma endregion

	bool captureMouse = false;

	void addPostProccessor(PostProcessingShader *);

	Camera * getCamera() {return gameCamera;}


	SceneRenderer* getRenderEngine() { return renderEngine; }
	PhysicsHandler* getPhysicsEngine() { return physicsEngine; }

	void updatePhysicsModel() { physicsEngine->checkScene(scenes[currScene]); }

	DisplayDetails * getDisplayDetails() { return thisConfig.displayDetails; }

private:
	Game();
	~Game();
	static Game * thisPointer;

	SceneRenderer* renderEngine = nullptr;
	PhysicsHandler * physicsEngine = nullptr;

	bool physicsFrame = true;

#pragma region GameObjects and Arrays of Gameobjects
	//The Pointer to the player
	GameObject* player = nullptr;

	ShadowBox* currShadowBox = nullptr;
#pragma endregion

#pragma region Object Containers
	//The keyState
	Keystate keys = 0;

	//The current game clock
	Clock* gameClock = nullptr;
	//The Main camera
	Camera * gameCamera = nullptr;
	//Scene Management stuff
	const static int MAX_SCENES =8 ;
	Scene * scenes[MAX_SCENES] = { nullptr };
	int currScene = 0;

#pragma endregion

#pragma region Sounds
	ALuint sound1;

	ALuint source1;


#pragma endregion

#pragma region bools
	bool mouseDownBool = false;
	bool playing = false;
	
	bool mouseOnHold = true;
#pragma endregion

#pragma region	Mouse Variables
	int mousePrevX, mousePrevY;
	double deltaTime = 0.0;
	int diffX = 0;
	int diffY = 0;
	int xSinceReset = 0;
	int ySinceReset = 0;
	int resetThreshold = 300;
#pragma endregion

#pragma region Internal Calls
	//BELOW--- Update calls

	//Update the camera variables
	void cameraUpdate();
	//update public behaviours
	void updateBehaviours();
	//Update each gameobject
	void gameObjectUpdate();
	//update transparent objects
	void transparentUpdate();
	//update reflectives
	void reflectiveUpdate();
	//upodate the lighting
	void lightingUpdate();
	//update GUI
	void guiUpdate();

	//BELOW-- render calls

#pragma endregion

#pragma region Straight Up Data
	glm::mat4 cameraMatrix;
#pragma endregion

};

