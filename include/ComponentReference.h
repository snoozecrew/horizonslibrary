#pragma once
class PhysicsComponent;
class GameObject;
class TerrainCollider;
class ComponentReference
{
public:

	//This class stores pointers to essential components such as physics. It may be used as a parent class to add more component functionality
	PhysicsComponent * physics = nullptr;
	TerrainCollider * terrainCollider = nullptr;
};

