#pragma once
#include "GameObject.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtc/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtc/matrix_transform.hpp>
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtx/transform.hpp>
#include <stdio.h>
#include <vector>
#include "tiny_obj_loader.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <FreeImage\FreeImagePlus.h>
#include "texture_loader.h"
#include "VAOData.h"
#include "Volume.h"



class Object3D : public virtual GameObject
{
public:
	Object3D();
	~Object3D() {}
	void setVAOData(VAOData* newData) { dataVAO = newData; }
	void draw(RenderSettings*);

	void update(double deltaTime);
	void lateUpdate(double deltaTime);
	void setShinyness(float newShiny) { shinyness = newShiny; }

	void setShaders(DefaultShaders* x) { defaultShaders = x; }
	 
protected:

	double deltaTime;

	//Transform Matricie



	float shinyness = 50.0f;

	DefaultShaders * defaultShaders =nullptr;
	
	void drawNormals(glm::mat4 camera);
	void drawTangents(glm::mat4 camera);
	
};

