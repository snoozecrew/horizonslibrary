#pragma once

class Behaviour
{
public:
	Behaviour() {}
	~Behaviour() {}

	void setActive(bool x) { active = x; }

	virtual void update(double f) = 0;
protected:

	bool active = true;
};