#pragma once
#include "FBO.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#include "Object3D.h"
class ReflectiveObject : public Object3D
{
public:
	ReflectiveObject() {}

	int getFBOCount() { return thisFBO->getTotalPasses(); }
	void bindFBOByIndex(int i) { thisFBO->bindFBOByIndex(i); }
	glm::mat4 getCameraAngleRequiredByIndex(int i) { return thisFBO->getCameraAngleRequiredByIndex(i); }
	bool getClipBoolByIndex(int i) { return thisFBO->getClipPlaneUseBoolByIndex(i); }
	glm::vec4 getClipPlaneByIndex(int i) { return thisFBO->getClipPlaneByIndex(i); }


	void unBind() { thisFBO->unBind(); }
	virtual void draw(RenderSettings*) {}



protected:
	FBOGroup * thisFBO;
	
	virtual void intializeFBOGroup() = 0;
};

