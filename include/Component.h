#pragma once
class GameObject;
class Component
{
public:
	Component(GameObject * h) { host = h; }
	~Component() {}

	GameObject* getHost() { return host; }

protected:
	GameObject * host = nullptr;
};

