#pragma once
#include "Camera.h"
class FirstPersonCamera :
	public Camera
{
public:
	FirstPersonCamera(GameObject * player);
	~FirstPersonCamera();

	void update(double deltaTimed,int,int, bool playing);
	glm::mat4 getLookAtMatrix(bool invertY, glm::vec3 pos);
protected:
	float y = 0.0f;

	glm::vec3 lookingAtConst = glm::vec3(0.0f, -5.0f, 0.0f);
	glm::vec3 targetAdjust = glm::vec3(0, 3, 0);

	glm::vec4 positionOffset = glm::vec4(0, 4, -4, 0);
	
	float mouseSens = 500.0f;
	float maxX = 0.6f;

	float minX = -0.6f;
}; 

