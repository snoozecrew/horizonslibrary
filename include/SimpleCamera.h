#pragma once
#include "Camera.h"
class SimpleCamera :
	public Camera
{
public:
	SimpleCamera();
	~SimpleCamera();

	void update(double delta, int mouseX, int mouseY, bool playing);

	glm::mat4 getLookAtMatrix(bool invertY, glm::vec3 pos);

	void setTarget(glm::vec3 x) { cameraTarget = x; }

	void setPos(glm::vec3 x) { cameraPos = x; }
private:
	
	
	float angle = 0.0f;

	const float speed = 0.0001f;
	const float radius = 20.0f;
};

