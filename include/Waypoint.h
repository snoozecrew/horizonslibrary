#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#include <string>


class Waypoint
{
public:
	Waypoint(glm::vec3 x, std::string n) { pos = x; name = n; }
	~Waypoint() {}

	glm::vec3 getPos() { return pos; }
	std::string getName() { return name; }

private:
	glm::vec3 pos = glm::vec3(0);
	std::string name;
};