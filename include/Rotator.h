#pragma once
#include "GameObject.h"
#include "PrivateBehaviour.h"
class yRotator :
	public PrivateBehaviour
{
public:
	yRotator(GameObject* newTarget, float newSpeed) { target = newTarget; speed = newSpeed; }
	~yRotator();

	void update(double delta) { target->setRot(glm::vec3(target->getRot().x, target->getRot().y + speed * delta, target->getRot().z)); }
	void draw(RenderSettings*) {}
	Volume * getVolume() { return nullptr; }
private:
	GameObject * target;
	float speed = 0.0f;
};


class xRotator :
	public PrivateBehaviour
{
public:
	xRotator(GameObject* newTarget, float newSpeed) { target = newTarget; speed = newSpeed; }
	~xRotator();

	void update(double delta) { target->setRot(glm::vec3(target->getRot().x + speed * delta, target->getRot().y, target->getRot().z)); }
	void draw(RenderSettings*) {}
	Volume * getVolume() { return nullptr; }
private:
	GameObject * target;
	float speed = 0.0f;
};
