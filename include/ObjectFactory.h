#pragma once
#include "GameObject.h"
#include "VAOData.h"
#include "TerrainTextureData.h"
#include "Terrain.h"
#include "Triangle.h"
#include "Bounds.h"
#include "Extents.h"

class ObjectFactory
{
public:
	static ObjectFactory * getInstance();

	virtual Terrain * makeTerrain(VAOData * v, TerrainTextureData * t, float tile, float scale);
	virtual GameObject * makeInstancedObject(InstancedVAOData *);
	virtual GameObject * makeCustomObject(VAOData*, glm::vec3 pos, glm::vec3 scale, glm::vec3 rot);
	virtual GameObject * makeCustomObject(VAOData*);

	virtual Volume * makeTerrainCollider(VAOData*,Bounds2D bounds, Terrain*);
	virtual Volume * makeBoxCollider(Extents, GameObject*);
	virtual Volume * makeCylinderCollider(Extents, GameObject*);
	virtual Volume * makeTrackCollider(MeshData* data, Bounds2D bounds);

	std::vector<Triangle> tris;

protected:

	ObjectFactory();
	~ObjectFactory();

	static ObjectFactory* thisPointer;




};

